import { ZONES, FIRST_YEAR, LAST_YEAR, ALL_ZONES, ALL_YEARS, VALID_TYPES, SELECT_ZONE_ID, SELECT_YEAR_ID, CURRENT_YEAR } from './config.js'
import { getPublicHolidays } from './api.js'
import { formatDate, flushResults } from './tools.js'

const PUBLIC_HOLIDAYS = {}

async function fetchData (zone) {
  if (zone in PUBLIC_HOLIDAYS) return
  PUBLIC_HOLIDAYS[zone] = await getPublicHolidays(zone)
}

function filterResults (data, year) {
  /**
   * Return a dictionnary
   * {
   *    YYYY-MM-DD: date name
   * }
   */
  return Object.keys(data).reduce((reducer, date) => {
    const dateYear = new Date(date).getFullYear()
    if (year === ALL_YEARS || dateYear === parseInt(year)) reducer[date] = data[date]
    return reducer
  }, {})
}

function orderByYear (data, year) {
  /**
   * Return a dictionnary of dictionnary
   * {
   *    [year]: {
   *        YYYY-MM-DD: date name
   *    }
   * }
   */
  if (year !== ALL_YEARS) return { [year]: filterResults(data, year) }
  return Object.keys(data).reduce((reducer, date) => {
    const dateYear = new Date(date).getFullYear()
    if (!(dateYear in reducer)) reducer[dateYear] = {}
    reducer[dateYear][date] = data[date]
    return reducer
  }, {})
}

function updateTitle (text) {
  const main = document.getElementsByTagName('main')[0]
  const title = [...main.children].find(el => el.classList.contains('title'))

  // Update title
  title.appendChild(document.createTextNode(text))
}

async function updateSubtitle (zone) {
  if (zone === ALL_ZONES) return

  const main = document.getElementsByTagName('main')[0]
  const subtitle = [...main.children].find(el => el.classList.contains('subtitle'))

  await fetchData(zone)

  // Determine the next public holiday for a specific zone
  const currentDate = new Date()
  let nextPublicHoliday = null
  for (const dateText in PUBLIC_HOLIDAYS[zone]) {
    const date = new Date(dateText)
    if (!nextPublicHoliday || (date > currentDate && nextPublicHoliday > date)) nextPublicHoliday = date
  }
  subtitle.appendChild(document.createTextNode(`Prochain jour férié : ${formatDate(nextPublicHoliday)} ${nextPublicHoliday.getFullYear()}`))
}

function updateColumns (data) {
  /**
   * Get a dictionnary of dictionnary
   * {
   *    [title]: {
   *        YYYY-MM-DD: date name
   *    }
   * }
   */
  const main = document.getElementsByTagName('main')[0]
  const columns = [...main.children].find(el => el.classList.contains('columns'))

  for (const title in data) {
    // Create a column with a panel
    const column = document.createElement('div')
    column.classList.add('column', 'is-one-quarter')
    columns.appendChild(column)

    const panel = document.createElement('div')
    panel.classList.add('panel')
    column.appendChild(panel)

    // Update panel title
    const panelHeader = document.createElement('p')
    panelHeader.classList.add('panel-heading')
    panelHeader.appendChild(document.createTextNode(title))
    panel.appendChild(panelHeader)

    // Add a panel block for each date
    for (const date in data[title]) {
      const panelBlock = document.createElement('div')
      panelBlock.classList.add('panel-block')
      panelBlock.appendChild(document.createTextNode(`${formatDate(date)} (${data[title][date]})`))
      panel.appendChild(panelBlock)
    }
  }
}

async function updateZone (zone, year) {
  updateTitle(ZONES[zone])
  updateSubtitle(zone)
  // Get all public holidays for a zone and filter it by year
  await fetchData(zone)
  updateColumns(orderByYear(PUBLIC_HOLIDAYS[zone], year))
}

async function updateYear (zone, year) {
  const allZonesSelected = zone === ALL_ZONES
  const [zones, title] = allZonesSelected ? [Object.keys(ZONES), year] : [[zone], ZONES[zone]]
  updateTitle(title)
  updateSubtitle(zone)
  // Update the results with the public holidays of each zone
  for (const z of zones) {
    const panelTitle = allZonesSelected ? ZONES[z] : year
    await fetchData(z)
    updateColumns({ [panelTitle]: filterResults(PUBLIC_HOLIDAYS[z], year) })
  }
}

function submit (event) {
  event.preventDefault()

  flushResults()

  // Zone option
  const selectZone = document.getElementById(SELECT_ZONE_ID)
  const selectedZone = selectZone.value
  const allZoneOption = [...selectZone.children].find(el => el.value === ALL_ZONES)

  // Year option
  const selectYear = document.getElementById(SELECT_YEAR_ID)
  const selectedYear = selectYear.value
  const allYearOption = [...selectYear.children].find(el => el.value === ALL_YEARS)

  // Cannot select 'All zones' and 'All years' at the same time
  if (allZoneOption.selected && allYearOption.selected) {
    // This should never happen
    return
  } else if (allZoneOption.selected || allYearOption.selected) {
    allYearOption.setAttribute('disabled', '')
    allZoneOption.setAttribute('disabled', '')
  } else {
    allZoneOption.removeAttribute('disabled')
    allYearOption.removeAttribute('disabled')
  }

  const method = allZoneOption.selected ? updateYear : updateZone
  method(selectedZone, selectedYear)
}

function addOptions (selectId, options) {
  if (!Object.keys(VALID_TYPES).includes(selectId)) return
  const DEFAULT_OPTION = VALID_TYPES[selectId]

  const select = document.getElementById(selectId)

  // Add a default option
  const option = document.createElement('option')
  option.setAttribute('selected', '')
  option.setAttribute('disabled', '')
  option.appendChild(document.createTextNode(DEFAULT_OPTION))
  select.appendChild(option)

  // Add an option for each zone/year
  for (const opt in options) {
    const option = document.createElement('option')
    option.appendChild(document.createTextNode(options[opt]))
    option.setAttribute('value', opt)
    select.appendChild(option)
  }

  select.addEventListener('change', submit)
}

function updateSelectOptions () {
  // Add zone options
  addOptions(SELECT_ZONE_ID, ZONES)

  // Add year options
  const yearValues = {}
  for (let year = FIRST_YEAR - 1; year < LAST_YEAR; year++) yearValues[year] = year
  addOptions(SELECT_YEAR_ID, yearValues)

  // Select current year by default
  const select = document.getElementById(SELECT_YEAR_ID)
  select.value = CURRENT_YEAR
  select.dispatchEvent(new Event('change'))
}

document.addEventListener('DOMContentLoaded', () => {
  updateSelectOptions()
})
