import { BASE_URL } from './config.js'

export const getPublicHolidays = async (zone) => {
  const response = await fetch(`${BASE_URL}/${zone}.json`)
  return response.json()
}
