function capitalize (string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

export const formatDate = (dateText) => {
  const date = new Date(dateText)
  const options = { weekday: 'long', month: 'long', day: 'numeric' }
  const formatedDate = new Intl.DateTimeFormat('fr-FR', options).format(date)
  return capitalize(formatedDate)
}

export const flushResults = () => {
  const main = document.getElementsByTagName('main')[0]
  // Remove all main grandchildren
  for (const element of main.children) {
    while (element.firstChild) {
      element.firstChild.remove()
    }
  }
}
