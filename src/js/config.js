export const BASE_URL = 'https://calendrier.api.gouv.fr/jours-feries'
export const ZONES = {
  'alsace-moselle': 'Alsace-Moselle',
  guadeloupe: 'Guadeloupe',
  guyane: 'Guyane',
  'la-reunion': 'La Réunion',
  martinique: 'Martinique',
  mayotte: 'Mayotte',
  metropole: 'Métropole',
  'nouvelle-caledonie': 'Nouvelle-Calédonie',
  'polynesie-francaise': 'Polynésie française',
  'saint-barthelemy': 'Saint-Barthélemy',
  'saint-martin': 'Saint-Martin',
  'saint-pierre-et-miquelon': 'Saint-Pierre-et-Miquelon',
  'wallis-et-futuna': 'Wallis-et-Futuna'
}

/*
 * According to the official documentation:
 *      https://api.gouv.fr/documentation/jours-feries
 * We can retrieve the french public holidays 20 years in the past and 5 years in the future.
 */
export const CURRENT_YEAR = new Date().getFullYear()
export const FIRST_YEAR = CURRENT_YEAR - 20
export const LAST_YEAR = CURRENT_YEAR + 5

export const ALL_ZONES = 'Toutes les zones'
export const ALL_YEARS = 'Toutes les années'

export const SELECT_ZONE_ID = 'select-zone'
export const SELECT_YEAR_ID = 'select-year'

export const VALID_TYPES = { [SELECT_ZONE_ID]: ALL_ZONES, [SELECT_YEAR_ID]: ALL_YEARS }
