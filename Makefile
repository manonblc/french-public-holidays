.PHONY: release

release:
	# Grep the version from package.json, squeeze multiple spaces, delete double/single quotes and commas, get 3rd val.
	# This command tolerates multiple whitespace sequences around the version number.
	$(eval version:=$(shell grep -m 1 version package.json | tr -s ' ' | tr -d '"' | tr -d "'" | tr -d ',' | cut -d' ' -f3))
	echo Releasing version $(version)
	git commit package.json -m "Version $(version)"
	git tag $(version)
	git push origin master $(version)

