import { ALL_ZONES, ZONES, ALL_YEARS } from '../../src/js/config.js'

describe('Initialization tests', () => {
  beforeEach(() => {
    // Mock current date
    cy.clock(new Date(2020, 2), ['Date'])

    cy.visit('../../src/index.html')
  })

  it('Check zone options', () => {
    let i = 0
    // Expected values
    const values = Object.values(ZONES)
    values.unshift(ALL_ZONES)

    // Check option value
    cy.get('#select-zone').children().each($el => {
      cy.wrap($el).should('have.text', values[i])
      i++
    })
  })

  it('Check year options', () => {
    let i = 0
    // Expected values
    const values = [ALL_YEARS]
    for (let year = 2000 - 1; year < 2025; year++) values.push(year)

    // Check option value
    cy.get('#select-year').children().each($el => {
      cy.wrap($el).should('have.text', values[i])
      i++
    })
  })

  it('Check attributes of the default options', () => {
    // Check "all zones" option
    cy.get('#select-zone option:first').should('be.disabled').should('be.selected')

    // Check "all years" option
    cy.get('#select-year option:first').should('be.disabled').should('not.be.selected')
    cy.get('#select-year option[value=2020]').should('be.selected')
  })
})
