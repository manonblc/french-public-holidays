import { ZONES, BASE_URL, ALL_YEARS } from '../../src/js/config.js'

describe('Selection tests', () => {
  beforeEach(() => {
    // Mock API
    cy.fixture('zone.json').then(zoneFixture => {
      cy.intercept(`${BASE_URL}/*.json`, zoneFixture).as('zone')
    })

    // Mock current date
    cy.clock(new Date(2019, 2), ['Date'])

    cy.visit('../../src/index.html')
  })

  it('Select a year', () => {
    const ZONES_KEYS = Object.keys(ZONES)

    const selectedYear = '2020'

    // Year is selected by default, wait that data are received
    for (let i = 0; i < ZONES_KEYS.length; i++) cy.wait('@zone')
    cy.get('main .columns').children().should('have.length', ZONES_KEYS.length)

    // Select another year
    cy.get('#select-year').select(selectedYear)

    // Check "all *" options
    cy.get('#select-zone option:first').should('be.disabled')
    cy.get('#select-year option:first').should('be.disabled')

    // Check title and subtitle
    cy.get('main .title').should('have.text', selectedYear)
    cy.get('main .subtitle').should('have.text', '')

    // Check the number of column
    cy.get('main .columns').children().should('have.length', ZONES_KEYS.length)

    // Check the number of row for each column
    cy.get('main .columns .column').each($el => {
      cy.wrap($el).children().children('.panel-block').should('have.length', 11)
    })
  })

  it('Unselect a year', () => {
    const ZONES_KEYS = Object.keys(ZONES)

    const selectedZone = ZONES_KEYS[Math.floor(Math.random() * ZONES_KEYS.length)]

    // Year is selected by default, wait that data are received
    for (let i = 0; i < ZONES_KEYS.length; i++) cy.wait('@zone')
    cy.get('main .columns').children().should('have.length', ZONES_KEYS.length)

    // Select a zone and unselect the year
    cy.get('#select-zone').select(ZONES[selectedZone])
    cy.get('#select-year').select(ALL_YEARS)

    // Check "all *" options
    cy.get('#select-zone option:first').should('be.disabled')
    cy.get('#select-year option:first').should('be.disabled')

    // Check title and subtitle
    cy.get('main .title').should('have.text', ZONES[selectedZone])
    cy.get('main .subtitle').should('have.text', 'Prochain jour férié : Mercredi 1 janvier 2020')

    // Check the number of column
    cy.get('main .columns').children().should('have.length', 4)

    // Check the title and the number of row for each column
    let i = 0
    const expected = [
      { nbRow: 11, year: 2020 },
      { nbRow: 2, year: 2023 },
      { nbRow: 1, year: 2024 },
      { nbRow: 4, year: 2025 }
    ]
    cy.get('main .columns .column').each($el => {
      cy.wrap($el).children().children('.panel-heading').should('have.text', expected[i].year)
      cy.wrap($el).children().children('.panel-block').should('have.length', expected[i].nbRow)
      i++
    })
  })
})
