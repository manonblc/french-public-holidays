import { ZONES, BASE_URL } from '../../src/js/config.js'

describe('Selection tests', () => {
  beforeEach(() => {
    // Mock API
    cy.fixture('zone.json').then(zoneFixture => {
      cy.intercept(`${BASE_URL}/*.json`, zoneFixture).as('zone')
    })

    // Mock current date
    cy.clock(new Date(2019, 2), ['Date'])

    cy.visit('../../src/index.html')
  })

  it('Select a zone and then a year', () => {
    const ZONES_KEYS = Object.keys(ZONES)

    // Mock API
    const selectedYear = '2020'
    const selectedZone = ZONES_KEYS[Math.floor(Math.random() * ZONES_KEYS.length)]

    // Year is selected by default, wait that data are received
    for (let i = 0; i < ZONES_KEYS.length; i++) cy.wait('@zone')
    cy.get('main .columns').children().should('have.length', ZONES_KEYS.length)

    // Select a zone and then another year
    cy.get('#select-zone').select(ZONES[selectedZone])
    cy.get('#select-year').select(selectedYear)

    // Check "all *" options are not disabled
    cy.get('#select-zone option:first').should('not.be.disabled')
    cy.get('#select-year option:first').should('not.be.disabled')

    // Check title and subtitle
    cy.get('main .title').should('have.text', ZONES[selectedZone])
    cy.get('main .subtitle').should('have.text', 'Prochain jour férié : Mercredi 1 janvier 2020')

    // Check the number of column
    cy.get('main .columns').children().should('have.length', 1)

    // Check the number of row for each column
    cy.get('main .columns .column').each($el => {
      cy.wrap($el).children().children('.panel-heading').should('have.text', selectedYear)
      cy.wrap($el).children().children('.panel-block').should('have.length', 11)
    })
  })

  it('Select a year and then a zone', () => {
    const ZONES_KEYS = Object.keys(ZONES)

    // Mock API
    const selectedYear = '2020'
    const selectedZone = ZONES_KEYS[Math.floor(Math.random() * ZONES_KEYS.length)]

    // Year is selected by default, wait that data are received
    for (let i = 0; i < ZONES_KEYS.length; i++) cy.wait('@zone')
    cy.get('main .columns').children().should('have.length', ZONES_KEYS.length)

    // Select another year and then a zone
    cy.get('#select-year').select(selectedYear)
    cy.get('#select-zone').select(ZONES[selectedZone])

    // Check "all *" options are not disabled
    cy.get('#select-zone option:first').should('not.be.disabled')
    cy.get('#select-year option:first').should('not.be.disabled')

    // Check title and subtitle
    cy.get('main .title').should('have.text', ZONES[selectedZone])
    cy.get('main .subtitle').should('have.text', 'Prochain jour férié : Mercredi 1 janvier 2020')

    // Check the number of column
    cy.get('main .columns').children().should('have.length', 1)

    // Check the number of row for each column
    cy.get('main .columns .column').each($el => {
      cy.wrap($el).children().children('.panel-heading').should('have.text', selectedYear)
      cy.wrap($el).children().children('.panel-block').should('have.length', 11)
    })
  })
})
