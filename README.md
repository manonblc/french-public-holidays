French public holidays
======================

Frontend application that retrieves the French public holidays from the [official api](https://api.gouv.fr/documentation/jours-feries).
Display the public holidays according to the selected zone or year and the next public holiday for a specific zone.

## Docker

If you want to run the application without installing it use [docker](https://www.docker.com). Run the following commands to build and run a container:

```
docker build . -t french-public-holidays
docker run -p 8080:8080 french-public-holidays
```

The application will be available at http://localhost:8080.

## Local development

### Installation

To install the application with the necessary packages, run:

```
npm install
```

### Setup server

To setup a local server run:

```
npm start
```

The application will be available at http://localhost:8080.

### Tests

You can run several types of tests.

#### Linting

To format files with [eslint](https://eslint.org) run:

```
npm run lint
```

#### Cypress

To run the [cypress](https://www.cypress.io) tests manually, run:

```
npm run test
```

Or you can run it automatically, with:

```
npx cypress run --browser firefox
```

If you want to see the progress of your tests during the CI, go to `https://dashboard.cypress.io/projects/97tr18`.

#### Vulnerabilities

To check javascript vulnerabilities run:

```
npm audit
```
