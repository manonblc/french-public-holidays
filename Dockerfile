FROM node:latest

WORKDIR /src

COPY /src ./src
COPY /package.json package.json

RUN npm install --only=prod

CMD [ "npm", "start" ]
